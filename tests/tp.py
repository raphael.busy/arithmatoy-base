# Aucun n'import ne doit être fait dans ce fichier

def S(n: str) -> str:
    return f"S{n}"

def nombre_entier(n: int) -> str:
    if (n == 0):
        return "0"
    if (n > 0):
        return S(nombre_entier(n - 1))

def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    if a.startswith("S"):
        return S(addition(a[1:], b))

def multiplication(a: str, b: str) -> str:
    if a == "0":
        return "0"
    if a.startswith("S"):
        return addition(b, multiplication(a[1:], b))

def facto_ite(n: int) -> int:
    i = 1
    f = 1
    while i <= n:
        f = f * i
        i = i + 1
    return f


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    return n * facto_rec(n - 1)

def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fibo_rec(n - 1) + fibo_rec(n - 2)

def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    if n == 1:
        return 1
    i = 0
    j = 1
    result = 0
    while n - 2 >= 0:
        result = i + j
        i = j
        j = result
        n = n - 1
    return result


def golden_phi(n: int) -> int:
    return (n + 5 ** 0.5) / 2

def sqrt5(n: int) -> int:
    i = 1
    while i < n / 2:
        if i * i == n:
            return i
        i += 1
    return 0

def pow(a: float, n: int) -> float:
    return a ** n
