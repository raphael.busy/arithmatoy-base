#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 1;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

int  e2600_strlen(const char *str)
{
  int i = 0;

  while (*(str + i))
      i++;
  return i;
}

char e2600_last_char(const char *str, int i)
{
  if (i > e2600_strlen(str))
    return 48;
  while(*(str + i))
    str++;
  return *str;
}

int get_value_from(char c, char const *all_digits)
{
  int i = 0;
  
  if (c == 0)
    return 0;
  while (all_digits[i]) {
    if (all_digits[i] == c)
      break;
      i++;
  }
  return i;
}


char e2600_last_char2(const char *str, int i, const char *all_digits)
{
  if (i > e2600_strlen(str))
    return 0;
  while(*(str + i))
    str++;
  return get_value_from(*str, all_digits);
}


int e2600_retenue_spot(const char *str) {
  if (*str == '9')
    return 1;
  return 0;
}

int e2600_biggest_nb(int a, int b)
{
  if (a >= b)
    return a;
  return b;
}

void arithmatoy_free(char *number) { free(number); }

int global = 0;

void  recursive_add(char buffer[], const char *lhs, const char *rhs, int const base, int buffer_len, int i, int *retenue, const char *all_digits)
{
  int value = *retenue + e2600_last_char2(lhs, i, all_digits) + e2600_last_char2(rhs, i, all_digits);

  if (VERBOSE) {
      fprintf(stderr, "add: digit %c digit %c carry %d\n", all_digits[e2600_last_char2(lhs, i, all_digits)], all_digits[e2600_last_char2(rhs, i, all_digits)], *retenue);
  }

  *retenue = 0;
  if (value >= base) {
    *retenue = 1;
    buffer[i - 1] = all_digits[value - base];
    if (VERBOSE) {
      fprintf(stderr, "add: result: digit %c carry %d\n", buffer[i - 1], *retenue);
    }
  }
  else {
    buffer[i - 1] = all_digits[value];
    if (VERBOSE) {
      fprintf(stderr, "add: result: digit %c carry %d\n", buffer[i - 1], *retenue);
    }
  }
  if (i >= buffer_len - 1) {
    if (VERBOSE) {
      fprintf(stderr, "final carry %d\n", *retenue);
    }
    if (*retenue == 1) {
      buffer[i] = '1';
    }
    return;
  }
  recursive_add(buffer, lhs, rhs, base, buffer_len, i + 1, retenue, all_digits);
}

void  recursive_sub(char buffer[], const char *lhs, const char *rhs, int const base, int buffer_len, int i, int *retenue, int sign, const char *all_digits)
{
  int value = *retenue + e2600_last_char2(lhs, i, all_digits) - e2600_last_char2(rhs, i, all_digits);

  int retenu = 0;
  if (*retenue == -1)
    retenu = 1;
  if (VERBOSE) {
      fprintf(stderr, "sub: digit %c digit %c carry %d\n", all_digits[e2600_last_char2(lhs, i, all_digits)], all_digits[e2600_last_char2(rhs, i, all_digits)], retenu);
  }

  *retenue = 0;
  if (value < 0)
  {
    (*retenue) = -1;
    buffer[i - 1] = all_digits[value + base];
    if (VERBOSE) {
      fprintf(stderr, "sub: result: digit %c carry 1\n", buffer[i - 1]);
    }
  }
  else {
    buffer[i - 1] = all_digits[value];
    if (VERBOSE) {
      fprintf(stderr, "sub: result: digit %c carry 0\n", buffer[i - 1]);
    }
  }
  if (buffer_len - i == 0 ) {
    return;
  }
  recursive_sub(buffer, lhs, rhs, base, buffer_len, i + 1, retenue, sign, all_digits);
}

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }
  int len_a = e2600_strlen(lhs);
  int len_b = e2600_strlen(rhs);
  int len_buffer = e2600_strlen(lhs) + 1;
  int retenue_spot = 1;

  char *buffer = malloc(len_buffer + retenue_spot + 1);
  int retenue = 0;

  recursive_add(buffer, lhs, rhs, base, len_buffer, 1, &retenue, get_all_digits());
  buffer = reverse(buffer);
  buffer = (char*)drop_leading_zeros(buffer);

  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
  return buffer;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }
  int is_second_string_bigger = 0;
  int len_a = e2600_strlen(lhs);
  int len_b = e2600_strlen(rhs);
  int len_buffer = e2600_biggest_nb(e2600_retenue_spot(lhs) + e2600_strlen(lhs), \
                   e2600_retenue_spot(rhs) + e2600_strlen(rhs));

/*  if (strcmp(lhs, rhs) > 0)
    is_second_string_bigger++;*/
  char *buffer = malloc(len_buffer + 1 + is_second_string_bigger);
  int retenue = 0;
/*  if (is_second_string_bigger) {
    recursive_sub(buffer, rhs, lhs, base, len_buffer, 1, &retenue, is_second_string_bigger, get_all_digits());
    buffer[0] = '-';
  }
  else*/
  recursive_sub(buffer, lhs, rhs, base, len_buffer, 1, &retenue, is_second_string_bigger, get_all_digits());
  buffer = reverse(buffer);
  buffer = (char*)drop_leading_zeros(buffer);
  buffer[len_buffer + is_second_string_bigger] = '\0';

  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
  return buffer;
}

char  *recursive_mul(char buffer[], const char *lhs, const char *rhs, int const base, int buffer_len, int i,const char *all_digits)
{
  int value = e2600_last_char2(rhs, i, all_digits);

  char *yolo = malloc(buffer_len + 1);
  *yolo = '0';

  while (value > 0) {
    yolo = arithmatoy_add(base, yolo, lhs);
    value--;
  }
  int j = e2600_strlen(yolo);
  int k = i;
  while (k > 1) {
    yolo[j++] = '0';
    k--;
  }
  if (i == strlen(rhs))
    return yolo;
  return arithmatoy_add(base, yolo, recursive_mul(buffer, lhs, rhs, base, buffer_len, i + 1, all_digits));
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  int len_a = e2600_strlen(lhs);
  int len_b = e2600_strlen(rhs);
  int sign = 1;
  int len_buffer = len_a + len_b;

  char *buffer = malloc(len_buffer + sign + 1);

  return recursive_mul(buffer, lhs, rhs, base, len_buffer, 1, get_all_digits());

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, "%s",debug_msg);
  exit(EXIT_FAILURE);
}
